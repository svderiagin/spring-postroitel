package com.epam;

@Deprecated
public class CoronaDesinfector {

    @InjectByType
    private Announcer announcer;
    @InjectByType
    private Policeman policemen;

    public void start(Room room) {
        announcer.announce("Начинаем дезинфекцию, вон все!");
        policemen.makePeopleLeaveRoom();
        desinfect(room);
        announcer.announce("Рискните зайти обратно");
    }

    private void desinfect(Room room) {
        System.out.println("Зачитывается молитва: корона изыди! - молитва прочитана, вирус извергнут в ад");
    }
}
